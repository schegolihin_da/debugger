<?php

class Mgt_DeveloperToolbar_Block_Tab_Layouts extends Mgt_DeveloperToolbar_Block_Tab
{
    public function __construct($name, $label)
    {
        parent::__construct($name, $label);
        $this->setTemplate('mgt_developertoolbar/tab/layouts.phtml');
    }
    
    public function getHandles()
    {
        return Mage::app()->getLayout()->getUpdate()->getHandles();
    }
}