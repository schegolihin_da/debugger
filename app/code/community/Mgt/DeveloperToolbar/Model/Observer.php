<?php
/**
 * MGT-Commerce GmbH
 * http://www.mgt-commerce.com
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@mgt-commerce.com so we can send you a copy immediately.
 *
 * @category    Mgt
 * @package     Mgt_DeveloperToolbar
 * @author      Stefan Wieczorek <stefan.wieczorek@mgt-commerce.com>
 * @copyright   Copyright (c) 2012 (http://www.mgt-commerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Mgt_DeveloperToolbar_Model_Observer extends Varien_Event_Observer
{
    public function removeCoreProfilerBlock($observer)
    {
        Mage::app()->getLayout()->removeOutputBlock('core_profiler');
    }

    public function addInfo($observer)
    {
        $layout = $observer->getEvent()->getLayout();
        $req  = Mage::app()->getRequest();
        Mage::register('debug_request',  $req->getRouteName());
        Mage::register('debug_full_action_name',  $req->getRequestedControllerName());
        Mage::register('debug_request_action_name',  $req->getRequestedActionName());
        //Mage::register('debug_handles',   implode("\n\t",$layout->getUpdate()->getHandles()));
        //Mage::register('debug_xml', $layout->getUpdate()->asString());
    }
    public function addHtml($observer)
    {
        $layout = $observer->getBlock()->getLayout();
        $block=$observer->getBlock();
        if ($block->getNameInLayout()=='after_body_start' ){
            $transport=$observer->getTransport();
            $html=$transport->getHtml();
            $content = $layout->createBlock(
                'komplizierte_debugger/tab',
                'debugger_data'
            );
            $transport->setHtml($html.$content->toHtml());
        }


    }
}